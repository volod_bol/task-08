package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Controller for file saving
 */

public class XMLSaverController {
    private static final String MEASURE = "measure";

    private XMLSaverController() {
        throw new IllegalStateException();
    }

    public static void writeXmlFile(List<Flower> flowers, String destination) throws ParserConfigurationException, TransformerException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        Document document = documentBuilder.newDocument(); // create xml document
        Element rootFlower = document.createElement("flowers"); // create and add element to xml doc
        rootFlower.setAttribute("xmlns", "http://www.nure.ua"); // set required attributes
        rootFlower.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootFlower.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
        document.appendChild(rootFlower);

        for (Flower flower : flowers) { // each attr of flower transforms to element and writes (adds\appends) to main rootFlower element
            Element elementFlower = document.createElement("flower");
            rootFlower.appendChild(elementFlower);

            Element name = document.createElement("name");
            name.setTextContent(flower.getName());
            elementFlower.appendChild(name);

            Element soil = document.createElement("soil");
            soil.setTextContent(flower.getSoil());
            elementFlower.appendChild(soil);

            Element origin = document.createElement("origin");
            origin.setTextContent(flower.getOrigin());
            elementFlower.appendChild(origin);

            Element visualParameters = document.createElement("visualParameters");

            Element stemColour = document.createElement("stemColour");
            stemColour.setTextContent(flower.getStemColour());
            visualParameters.appendChild(stemColour);

            Element leafColour = document.createElement("leafColour");
            leafColour.setTextContent(flower.getLeafColour());
            visualParameters.appendChild(leafColour);

            Element aveLenFlower = document.createElement("aveLenFlower");
            aveLenFlower.setAttribute(MEASURE, "cm");
            aveLenFlower.setTextContent(String.valueOf(flower.getAveLenFlower()));
            visualParameters.appendChild(aveLenFlower);

            elementFlower.appendChild(visualParameters);

            Element growingTips = document.createElement("growingTips");

            Element tempreture = document.createElement("tempreture");
            tempreture.setAttribute(MEASURE, "celcius");
            tempreture.setTextContent(String.valueOf(flower.getTemperature()));
            growingTips.appendChild(tempreture);

            Element lighting = document.createElement("lighting");
            lighting.setAttribute("lightRequiring", flower.getLightRequiring()); // for light need set attribute
            growingTips.appendChild(lighting);

            Element watering = document.createElement("watering");
            watering.setAttribute(MEASURE, "mlPerWeek");
            watering.setTextContent(String.valueOf(flower.getWateringPerWeek()));
            growingTips.appendChild(watering);

            elementFlower.appendChild(growingTips);

            Element multiplying = document.createElement("multiplying");
            multiplying.setTextContent(flower.getMultiplying());
            elementFlower.appendChild(multiplying);
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(document);
        FileWriter writer = new FileWriter(destination);
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
    }
}
